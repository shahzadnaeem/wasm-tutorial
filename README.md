# emscripten SDK Guide

## Installation

[Follow this guide](https://emscripten.org/docs/getting_started/downloads.html)

## Activation

```sh
shahzad: emsdk (main) $ ./emsdk activate latest

# output
Resolving SDK alias 'latest' to '3.0.0'
Resolving SDK version '3.0.0' to 'sdk-releases-upstream-7fbe748230f2ce99abbf975d9ad997699efb3153-64bit'
Setting the following tools as active:
   node-14.15.5-64bit
   releases-upstream-7fbe748230f2ce99abbf975d9ad997699efb3153-64bit

Next steps:
- To conveniently access emsdk tools from the command line,
  consider adding the following directories to your PATH:
    /home/shahzad/projects/emscripten/emsdk
    /home/shahzad/projects/emscripten/emsdk/node/14.15.5_64bit/bin
    /home/shahzad/projects/emscripten/emsdk/upstream/emscripten
- This can be done for the current shell by running:
    source "/home/shahzad/projects/emscripten/emsdk/emsdk_env.sh"
- Configure emsdk in your shell startup scripts by running:
    echo 'source "/home/shahzad/projects/emscripten/emsdk/emsdk_env.sh"' >> $HOME/.bash_profile


shahzad: wasm-tutorial (main) $ . ../emsdk/emsdk_env.sh

# output
Adding directories to PATH:
PATH += /home/shahzad/projects/emscripten/emsdk
PATH += /home/shahzad/projects/emscripten/emsdk/upstream/emscripten
PATH += /home/shahzad/projects/emscripten/emsdk/node/14.15.5_64bit/bin

...
```

## Tutorial

Simple 'hello world!' - see Makefile
