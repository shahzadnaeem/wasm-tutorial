# Compiler - must be accessible via PATH
EMCC=emcc

# PHP server to run demo in browser
HOST=localhost
PORT=5001


hello.html: hello.c
	$(EMCC) $< -o $@

serve: hello.html
	@ echo -e "\n\thttp://$(HOST):$(PORT)/$<\n"
	php -S $(HOST):$(PORT)
